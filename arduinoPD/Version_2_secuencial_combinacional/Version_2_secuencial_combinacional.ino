#include <Wire.h>
#include "MCP23008.h"

// Direcciones I2C de los componentes
#define DIR_I2C_GESTION 0x27
#define DIR_I2C_SENSORES 0x20

// Estados para la programación basada en autómatas
#define PARADO 0
#define CARRERA 1

// Pines del arduino
#define MOTOR_DER_DIR  4
#define MOTOR_DER_VEL  5
#define MOTOR_IZQ_DIR  12
#define MOTOR_IZQ_VEL  6

// Instancia de la placa de control
MCP23008 gestionI2C;
MCP23008 sensoresI2C;

// Variables
unsigned char estado = PARADO;

void setup() {

  Serial.begin(9600);

  pinMode(MOTOR_DER_DIR, OUTPUT);
  pinMode(MOTOR_DER_VEL, OUTPUT);
  pinMode(MOTOR_IZQ_DIR, OUTPUT);
  pinMode(MOTOR_IZQ_VEL, OUTPUT);

  gestionI2C.begin(DIR_I2C_GESTION);
  gestionI2C.pinMode(0x0F);
  gestionI2C.setPullup(0x0F);

  sensoresI2C.begin(DIR_I2C_SENSORES);
  sensoresI2C.pinMode(0xFF);
  sensoresI2C.setPullup(0x00);
}

void loop() {

  switch (estado) {
    case PARADO:
      if (botonGrandePulsado() == true) {
        gestionI2C.write(4, HIGH);
        estado = CARRERA;
        break;
      }
      break;
    case CARRERA:
      if (botonGrandePulsado() == true) {
        gestionI2C.write(4, LOW);
        estado = PARADO;
        digitalWrite(MOTOR_DER_DIR, LOW);
        digitalWrite(MOTOR_DER_VEL, LOW);
        digitalWrite(MOTOR_IZQ_DIR, LOW);
        digitalWrite(MOTOR_IZQ_VEL, LOW);
        break;
      }
      char values = sensoresI2C.read();
      Serial.println(values, BIN);
      switch (values) {
        case 0b1000:
          Serial.println("Izquierda mucho");
          digitalWrite(MOTOR_DER_DIR, LOW);
          analogWrite(MOTOR_DER_VEL, 120);
          digitalWrite(MOTOR_IZQ_DIR, LOW);
          analogWrite(MOTOR_IZQ_VEL, 0);
          break;
        case 0b1100:
          Serial.println("Izquierda poco");
          digitalWrite(MOTOR_DER_DIR, LOW);
          analogWrite(MOTOR_DER_VEL, 100);
          digitalWrite(MOTOR_IZQ_DIR, LOW);
          analogWrite(MOTOR_IZQ_VEL, 60);
          break;
          case 0b0100:
          Serial.println("Izquierda muy poco");
          digitalWrite(MOTOR_DER_DIR, LOW);
          analogWrite(MOTOR_DER_VEL, 95);
          digitalWrite(MOTOR_IZQ_DIR, LOW);
          analogWrite(MOTOR_IZQ_VEL, 65);
          break;
        case 0b0110:
          Serial.println("En linea");
          digitalWrite(MOTOR_DER_DIR, LOW);
          analogWrite(MOTOR_DER_VEL, 80);
          digitalWrite(MOTOR_IZQ_DIR, LOW);
          analogWrite(MOTOR_IZQ_VEL, 80);
          break;
          case 0b0010:
          Serial.println("Derecha muy poco");
          digitalWrite(MOTOR_DER_DIR, LOW);
          analogWrite(MOTOR_DER_VEL, 65);
          digitalWrite(MOTOR_IZQ_DIR, LOW);
          analogWrite(MOTOR_IZQ_VEL, 95);
          break;
        case 0b0011:
          Serial.println("Derecha poco");
          digitalWrite(MOTOR_DER_DIR, LOW);
          analogWrite(MOTOR_DER_VEL, 60);
          digitalWrite(MOTOR_IZQ_DIR, LOW);
          analogWrite(MOTOR_IZQ_VEL, 100);
          break;
        case 0b0001:
          Serial.println("Derecha mucho");
          digitalWrite(MOTOR_DER_DIR, LOW);
          analogWrite(MOTOR_DER_VEL, 0);
          digitalWrite(MOTOR_IZQ_DIR, LOW);
          analogWrite(MOTOR_IZQ_VEL, 120);
          break;
        default:
          Serial.println("Parado");
          digitalWrite(MOTOR_DER_DIR, LOW);
          digitalWrite(MOTOR_DER_VEL, LOW);
          digitalWrite(MOTOR_IZQ_DIR, LOW);
          digitalWrite(MOTOR_IZQ_VEL, LOW);
          break;
      }
      break;
  }
  delay(10);
}

unsigned char estado_anterior_boton = HIGH;

bool botonGrandePulsado() {

  // lectura del estado del botón
  char estado_boton = gestionI2C.read(3);
  // identificación de un flanco ascendente en base al estado anterior y al actual
  if ((estado_anterior_boton == HIGH) && (estado_boton == LOW)) {
    estado_anterior_boton = estado_boton;
    return true;
  }
  else {
    estado_anterior_boton = estado_boton;
    return false;
  }
}
