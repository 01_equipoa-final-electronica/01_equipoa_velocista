#include <Wire.h>
#include "MCP23008.h"

// Direcciones I2C de los componentes
#define DIR_I2C_GESTION 0x27

// Estados para la programación basada en autómatas
#define PARADO    0
#define CARRERA   1

// Instancia de la placa de control
MCP23008 gestionI2C;

// Variables
unsigned char estado = PARADO;

void setup() {

  gestionI2C.begin(DIR_I2C_GESTION);
  gestionI2C.pinMode(0x0F);
  gestionI2C.setPullup(0x0F);
}

void loop() {

  switch (estado) {
    case PARADO:
      if (botonGrandePulsado() == true) {
        gestionI2C.write(4, HIGH);
        estado = CARRERA;
        break;
      }
      break;
    case CARRERA:
      if (botonGrandePulsado() == true) {
        gestionI2C.write(4, LOW);
        estado = PARADO;
        break;
      }
      break;
  }
  delay(10);
}

unsigned char estado_anterior_boton = HIGH;

bool botonGrandePulsado() {

  // lectura del estado del botón
  char estado_Boton = gestionI2C.read(3);
  // identificación de un flanco ascendente en base al estado anterior y al actual
  if ((estado_anterior_boton == HIGH) && (estado_Boton == LOW)) {
    estado_anterior_boton = estado_Boton;
    return true;
  }
  else {
    estado_anterior_boton = estado_Boton;
    return false;
  }
}
